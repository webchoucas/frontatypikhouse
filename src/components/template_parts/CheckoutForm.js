import React, { useState } from "react";
import ReactDOM from "react-dom";
import {
  CardElement,
  Elements,
  CardNumberElement,
  CardExpiryElement,
  CardCvcElement,
  useStripe,
  useElements,
} from "@stripe/react-stripe-js";

const CARD_ELEMENT_OPTIONS = {
  iconStyle: "solid",
  hidePostalCode: true,
  style: {
    base: {
      iconColor: "rgb(240, 57, 122)",
      color: "rgb(240, 57, 122)",
      fontSize: "16px",
      fontFamily: '"Open Sans", sans-serif',
      fontSmoothing: "antialiased",
      "::placeholder": {
        color: "#CFD7DF",
      },
    },
    invalid: {
      color: "#e5424d",
      ":focus": {
        color: "#303238",
      },
    },
  },
};

const CheckoutForm = (props) => {
  let profil = props.profil;
  const stripe = useStripe();
  const elements = useElements();
  const [fields, setFields] = useState({
    name: profil.name,
    lastName: profil.lastName,
    email: profil.email,
    address: profil.rue + "," + profil.zipCode + " " + profil.city,
    persons: 1,
    card: "",
    date: "",
    cvc: "",
  });

  const [card, setCard] = useState({});

  // console.log(stripe);

  const handleChange = (e) => {
    setFields({ ...fields, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!stripe || !elements) {
      console.log("no stripe");
      return;
    }

    const card = elements.getElement(CardElement);
    const result = await stripe.createToken(card);
    if (result.error) {
      console.log(result.error.message);
      alert("Carte érronné");
    } else {
      props.setInfo(result.token);
      console.log(result.token);
    }
    props.nextStep({ ...fields });
  };

  const handleCard = (e) => {
    // console.log(e);
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="form-group">
        <label htmlFor="da">Name:</label>
        <input
          value={fields.name}
          type="text"
          name="name"
          onChange={handleChange}
          className="form-control"
        />
      </div>
      <div className="form-group">
        <label htmlFor="da">Prénom:</label>
        <input
          type="text"
          value={fields.lastName}
          name="lastName"
          onChange={handleChange}
          className="form-control"
        />
      </div>
      <div className="form-group">
        <label htmlFor="da">Email:</label>
        <input
          type="email"
          name="email"
          value={fields.email}
          onChange={handleChange}
          className="form-control"
        />
      </div>
      <div className="form-group">
        <label htmlFor="da">Address:</label>
        <input
          type="text"
          name="address"
          value={fields.address}
          onChange={handleChange}
          className="form-control"
        />
      </div>
      <div className="form-group">
        <label htmlFor="da">Number of person:</label>
        <input
          type="number"
          name="persons"
          onChange={handleChange}
          value={fields.persons}
          className="form-control"
        />
      </div>
      {/* <Elements stripe={stripePromise}> */}
      <div className="form-group">
        <label htmlFor="da">Information bancaire</label>
        <CardElement options={CARD_ELEMENT_OPTIONS} onChange={handleCard} />
      </div>
      {/* </Elements> */}
      <div className="form-group d-flex justify-content-around">
        <button onClick={props.prevStep} className="btn btn-light">
          Précédent
        </button>
        <button disabled={!stripe} className="btn btn-info" type="submit">
          Suivant
        </button>
      </div>
    </form>
  );
};

export default CheckoutForm;

// const App = () => (
//   <Elements stripe={stripePromise}>
//     <CheckoutForm />
//   </Elements>
// );
