import axios from "axios";
import { API_ROOT } from "../components/urls";

export const registerApi = (data) => {
  let link = API_ROOT
  let profil = axios
    .post(`${link}/users/register`, data)
    .then((res) => {
      const message = res.data;
      return message;
    })
    .catch((error) => {
      return (error = {
        error: error,
      });
    });
  return profil;
};
