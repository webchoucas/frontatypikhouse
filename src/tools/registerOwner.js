import axios from "axios";
import setAuthorizationToken from "./../tools/setAuthorizationToken";
import jwt from "jsonwebtoken";
import { API_ROOT } from "../components/urls";

export const registerOwnerApi = (data) => {
  let link = API_ROOT
  let owner = axios
    .post(`${link}/owner/register`, data)
    .then((res) => {
      const message = res.data;
      return message;
    })
    .catch((error) => {
      return (error = {
        error: error,
      });
    });
  return owner;
};
